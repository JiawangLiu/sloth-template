const webpack = require('webpack'),
    path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    autoprefixer = require('autoprefixer'),
    es3ifyPlugin = require('es3ify-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname, ".");
const BUILD_PATH = path.resolve(ROOT_PATH, 'build');


module.exports = {
    entry: {
        polyfill : 'babel-polyfill',
        main : './src/index.js'
    },
    output: {
        path: BUILD_PATH,
        filename: 'js/[name].js'
    },
    externals: {
        jquery: 'jQuery'
    },
    module: {
        loaders: [
            {test: /\.(js|jsx)(\?.*$|$)/,exclude: /node_modules/,loader: 'babel-loader'},
            {test: /\.(png|jpg|gif|bmp|svg|swf)(\?.*$|$)/, loader: "url?limit=2048&name=img/[hash:8].[ext]" },
            {test: /\.css$/,loader: ExtractTextPlugin.extract("style", "css?sourceMap=true")},
            {test: /\.scss$/,loader: ExtractTextPlugin.extract("style", "css?sourceMap=true!postcss!sass", {publicPath: "../"})},
        ]
    },
    postcss: function () {
        return [autoprefixer];
    },
    plugins: [
        new ExtractTextPlugin("./css/[name].css")
    ],
    devServer: {
        disableHostCheck: true,
        historyApiFallback: true,
        progress: true,
        outputPath : BUILD_PATH,
        host : "0.0.0.0",
        port:3000
    },
    devtool: 'source-map'
}

if(process.env.NODE_ENV === "ie8"){
    module.exports.plugins.push(new es3ifyPlugin())
}

let glob = require('glob');
glob.sync(path.resolve(ROOT_PATH, 'templates', '*.html')).forEach(function(tplPath){
    module.exports.plugins.push(new HtmlWebpackPlugin({template : "html?interpolate&minimize=false!"+tplPath,filename:path.basename(tplPath) ,hash : true}))
})