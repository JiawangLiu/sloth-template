const webpack = require('webpack'),
    path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    autoprefixer = require('autoprefixer'),
    es3ifyPlugin = require('es3ify-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname, ".");
const BUILD_PATH = path.resolve(ROOT_PATH, 'build');

module.exports = {
    entry: {
        polyfill : 'babel-polyfill',
        main : './src/index.js'
    },
    output: {
        path: BUILD_PATH,
        filename: 'js/[name].js'
    },
    externals: {
        jquery: 'jQuery'
    },
    module: {
        loaders: [
            {test: /\.(js|jsx)(\?.*$|$)/,exclude: /node_modules/,loader: 'babel-loader'},
            {test: /\.(png|jpg|gif|bmp|svg|swf)(\?.*$|$)/, loader: "url?limit=2048&name=img/[hash:8].[ext]" },
            {test: /\.css$/,loader: ExtractTextPlugin.extract("style", "css")},
            {test: /\.scss$/,loader: ExtractTextPlugin.extract("style", "css?sourceMap=true!postcss!sass", {publicPath: "../"})},
        ]
    },
    postcss: function () {
        return [autoprefixer];
    },
    plugins: [
        new es3ifyPlugin(),
        new ExtractTextPlugin("./css/[name].css"),
        new webpack.optimize.UglifyJsPlugin({mangle : false, output : {keep_quoted_props:true}, compress : {properties:false, drop_console: true},comments:false}),
        new CleanWebpackPlugin("build", {root:ROOT_PATH})
    ],
    devtool: 'source-map'
}

let glob = require('glob');
glob.sync(path.resolve(ROOT_PATH, 'templates', '*.html')).forEach(function(tplPath){
    module.exports.plugins.push(new HtmlWebpackPlugin({template : "html?interpolate&minimize=false!"+tplPath,filename:path.basename(tplPath) ,hash : true}))
})