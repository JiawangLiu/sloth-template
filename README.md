# Slothd Template
A default slothd template

## Usage

### Start

start with yarn(Recommend) or npm.

```bash
$ yarn/npm start
```

open http://127.0.0.1/ with your browser.

### IE8

if you want run in ie8

```bash
$ yarn ie8
```
### Build

build this project

```bash
$ yarn build
```