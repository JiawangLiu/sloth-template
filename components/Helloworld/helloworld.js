import Sloth from 'slothd';
import './helloworld.scss';

@Sloth.component({
    defaultOptions:{
        test: 'hello World!'
    }
})

export default class Helloworld {
    onInit() {
        alert(this.options.test);
    }
}

